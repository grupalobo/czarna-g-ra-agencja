$(document).ready(function(){     
    var scroll_pos = 0;
    $(document).scroll(function() { 
        scroll_pos = $(this).scrollTop();
        if(scroll_pos > 95) {
            $(".sidebar").removeClass('ontop').addClass('scroll');
        } else {
            $(".sidebar").removeClass('scroll').addClass('ontop');
        }
    });
});