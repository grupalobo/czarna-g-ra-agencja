$(document).ready(function(){
    $("#DOPBSPSearch1").addClass("visible").removeClass("hidden");
    $(".number-select").change(function() {
        if ($(".number-select").val() == 1){
            $("#DOPBSPSearch3").addClass("visible").removeClass("hidden");
            $("#DOPBSPSearch1").addClass("hidden").removeClass("visible");
            $("#DOPBSPSearch5").addClass("hidden").removeClass("visible");
            $("#DOPBSPSearch4").addClass("hidden").removeClass("visible");
        }
        else if ($(".number-select").val() == 2){
            $("#DOPBSPSearch4").addClass("visible").removeClass("hidden");
            $("#DOPBSPSearch1").addClass("hidden").removeClass("visible");
            $("#DOPBSPSearch5").addClass("hidden").removeClass("visible");
            $("#DOPBSPSearch3").addClass("hidden").removeClass("visible");
        }
        else if ($(".number-select").val() == 3){
            $("#DOPBSPSearch5").addClass("visible").removeClass("hidden");
            $("#DOPBSPSearch1").addClass("hidden").removeClass("visible");
            $("#DOPBSPSearch4").addClass("hidden").removeClass("visible");
            $("#DOPBSPSearch3").addClass("hidden").removeClass("visible");
        }
        else {
            $("#DOPBSPSearch1").addClass("visible").removeClass("hidden");
            $("#DOPBSPSearch4").addClass("hidden").removeClass("visible");
            $("#DOPBSPSearch3").addClass("hidden").removeClass("visible");
            $("#DOPBSPSearch5").addClass("hidden").removeClass("visible");
        }
    });
});

