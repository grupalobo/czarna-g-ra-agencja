$(document).ready(function(){     
    var scroll_pos = 0;
    $(document).scroll(function() { 
        scroll_pos = $(this).scrollTop();
        if(scroll_pos > 65) {
            $("nav[id='main-menu']").removeClass('ontop').addClass('scroll');
            $("div[id='sidebar']").removeClass('ontop').addClass('scroll');
            $("img[class='bright']").hide();
            $("img[class='dark']").show();
        } else {
            $("nav[id='main-menu']").removeClass('scroll').addClass('ontop');
            $("div[id='sidebar']").removeClass('scroll').addClass('ontop');
            $("img[class='bright']").show();
            $("img[class='dark']").hide();
        }
    });
});