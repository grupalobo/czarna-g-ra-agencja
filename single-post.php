<?php
/*
Template Name: Single Post
Template używany do wyświetlania pojedynczej nieruchomości
*/
    get_header();
    get_sidebar(cat);
?>
    <section class="content">

        <div class="right-column-switch">
            <div class="blue paragraph right-column-paragraph" style="padding-top: 88px; padding-bottom: 50px">
                <h2 class="if-small entry-title" style="position: absolute; left: 30px; top: 30px">
                    <?php the_title(); ?>
                </h2>
                <button class="orange regular price-button">
                    <?php get_estate_price() ?>
                </button>
                <div class="estate-contact-data">
                    <hr/>
                    <h2 class="with-list" style="margin-bottom: 30px">Dane kontaktowe</h2>
                        <span><strong>Adres:</strong></span>
                        <span>ul. Nowa 23</span>
                        <span>65-897 Poznań</span>
                        <span><strong>Email:</strong></span>
                        <span>firma@firma.poznan.pl</span>
                        <span><strong>Tel:</strong></span>
                        <span>345 678 234</span>
                        <span>343 123 543</span>
                    <hr class="between"/>
                </div>
                <h2 class="with-list" style="margin-bottom: 30px">Lokalizacja</h2>
                    <?php get_estate_location() ?>
            </div>
        </div>

        <div class="left-column-switch">
            <div class="blue paragraph">
                <h2 class="entry-title">
                    <?php the_title(); ?>
                </h2>
                <hr/>
                    <?php get_content(); ?>
            </div>
        </div>

    </section>

<?php get_footer(); ?>