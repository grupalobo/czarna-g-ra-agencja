<?php 

function register_my_menus(){
  register_nav_menus(
    array(
     'main-menu'=>__('Main Menu'),
     'footer-menu'=>__('Footer Menu'),
     'sidebar-menu'=>__('Sidebar Menu')
   )
 );
}

function get_content(){
	if (have_posts()) : while (have_posts()) : the_post();
    	the_content();
    endwhile; endif;
}

function get_post_thumbnail(){
    if ( has_post_thumbnail() ) {
    the_post_thumbnail();}
}

function get_estate_price(){
	$key_1_value = get_post_meta( get_the_ID(), 'cena', true );
    if ( ! empty( $key_1_value ) ) {
    echo $key_1_value;}                      
}

function get_estate_location(){
	$key_2_value = get_post_meta( get_the_ID(), 'lok', true );
    if ( ! empty( $key_2_value ) ) {
    echo $key_2_value;}
}

add_action('init', 'register_my_menus');

add_theme_support( 'post-thumbnails' ); 


?>