<!DOCTYPE html>
<html <?php language_attributes(); ?>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title(); ?></title>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/javascript/sidebar.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/javascript/menu.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/javascript/search.js"></script>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/buttons.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/core.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/elements.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/forms.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/sidebar-right.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/text.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/navigation.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/accomodation-search.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/booking.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/styles/estate.css" type="text/css" media="screen" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>

        <!-- Skrypty dotyczące pluginu rezerwacyjnego -->

        <script type="text/javascript">
          
          jQuery(function() {
            jQuery('.wpeb_search-widget_frm').each(function(){
              
              var widgetID =  jQuery(this).attr('class').replace('wpeb_search-widget_frm','');

              widgetID = widgetID.replace(' ','');
              var formClass = '.'+jQuery(this).attr('class').replace(' ','.');
              var widgetNum = widgetID.replace('wpeb-search-widget-', '');
              
              //var widgetNum =  jQuery(this).attr('data-wpeb-search-id');
              //var formClass =  widgetNum;
              
              
              var dates = jQuery( formClass+"  .wpeb_date_start, "+formClass+" .wpeb_date_end" ).datepicker({
                defaultDate: "+1w",
                dateFormat : 'dd M yy',
                changeMonth: true,
                numberOfMonths: 1,
                minDate: 0,
                onSelect: function( selectedDate ) {
                  //alert( this.getAttribute('class') );
                  //var option = this.id == "wpeb_date_start" ? "minDate" : "maxDate",
                  var minRmax_option = "maxDate";
                  /*if( this.getAttribute('class') == "wpeb_date_start" ) minRmax_option = "minDate";
                  if( this.getAttribute('class') == "wpeb_date_end" ) minRmax_option = "maxDate";*/
                  if( this.id == 'wpeb_date_start-'+widgetNum ) minRmax_option = "minDate";
                  if( this.id == 'wpeb_date_end-'+widgetNum ) minRmax_option = "maxDate";
                  //var option = this.getAttribute('class') == "wpeb_date_start" ? "minDate" : "maxDate",
                  var option = minRmax_option,
                    instance = jQuery( this ).data( "datepicker" ),
                    date = jQuery.datepicker.parseDate(
                      instance.settings.dateFormat ||
                      jQuery.datepicker._defaults.dateFormat,
                      selectedDate, instance.settings );
                  dates.not( this ).datepicker( "option", option, date );
                }
              });
            });
          });
        </script>

        <script type='text/javascript'>
                jQuery( document ).ready(function(){            
                    jQuery('#wpeb-search-form-3').on('submit', function( e ){   
                        if( jQuery('#wpeb_date_start-3').val() == '' || jQuery('#wpeb_date_end-3').val() == '' ){
                            e.preventDefault();
                            jQuery( '#empty-dates-msg-3' ).fadeIn(400).delay(5000).fadeOut(400);
                            return false;
                        }               
                    });
                });
        </script>

        <!-- Skrypty dotyczące pluginu rezerwacyjnego koniec -->

    </head>
    <body <?php body_class(); ?>>
    
        <?php
            $menu_settings = array(
                'theme_location'  => 'main-menu',
                'menu'            => '',
                'container'       => 'nav',
                'container_class' => '',
                'container_id'    => 'main-menu',
                'menu_class'      => '',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            );

            wp_nav_menu($menu_settings);
        ?>

        <div class="logo">
            <a href="<?php site_url(); ?>/"><img class="bright" src="<?php bloginfo('template_directory'); ?>/images/czarna_napis_logo_white.svg" height="60px" width="257px"></a>
            <a href="<?php site_url(); ?>/"><img class="dark" src="<?php bloginfo('template_directory'); ?>/images/czarna_napis_logo_black.svg" height="60px" width="257px"></a>
        </div>