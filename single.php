<?php
/*
Template Name: Single
Template używany do wyświetlania strony
*/

  get_header();

  global $resortMap; ?>
  <?php if( $resortMap != '' ): ?>

  <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
  <script>
    jQuery( document ).ready( function(){

          function displayMap() {
                      document.getElementById('map_canvas').style.display="block";
                      initialize();
                  }
           function initialize() {
                    // create the map
                var myLatlng = new google.maps.LatLng(<?php echo $resortMap; ?>);
                  var myOptions = {
                      zoom: 14,
                      center: myLatlng,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                      map = new google.maps.Map(document.getElementById("map_canvas"),
                                                  myOptions);
                    
                    var marker = new google.maps.Marker({
                      position: myLatlng,
                      map: map,
                      title: 'Hello World!'
                  });

                   }
                   displayMap();
                   });                             
  </script>

  <?php endif;

  get_sidebar();
?>
        <section class="content">
            <?php get_content();?>
        </section>

<?php get_footer(); ?>
