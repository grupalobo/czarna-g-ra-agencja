<?php
/* Template Name: Accomodation Search */ 
/* Template użytwany do wyświetlania strony noclegi */
?>
<?php get_header(); ?>

<!-- Skrypty dotyczące pluginu rezerwacyjnego -->

  <script type="text/javascript">
    
    jQuery(function() {
      jQuery('.wpeb_search-widget_frm').each(function(){
        
        var widgetID =  jQuery(this).attr('class').replace('wpeb_search-widget_frm','');

        widgetID = widgetID.replace(' ','');
        var formClass = '.'+jQuery(this).attr('class').replace(' ','.');
        var widgetNum = widgetID.replace('wpeb-search-widget-', '');
        
        //var widgetNum =  jQuery(this).attr('data-wpeb-search-id');
        //var formClass =  widgetNum;
        
        
        var dates = jQuery( formClass+"  .wpeb_date_start, "+formClass+" .wpeb_date_end" ).datepicker({
          defaultDate: "+1w",
          dateFormat : 'dd M yy',
          changeMonth: true,
          numberOfMonths: 1,
          minDate: 0,
          onSelect: function( selectedDate ) {
            //alert( this.getAttribute('class') );
            //var option = this.id == "wpeb_date_start" ? "minDate" : "maxDate",
            var minRmax_option = "maxDate";
            /*if( this.getAttribute('class') == "wpeb_date_start" ) minRmax_option = "minDate";
            if( this.getAttribute('class') == "wpeb_date_end" ) minRmax_option = "maxDate";*/
            if( this.id == 'wpeb_date_start-'+widgetNum ) minRmax_option = "minDate";
            if( this.id == 'wpeb_date_end-'+widgetNum ) minRmax_option = "maxDate";
            //var option = this.getAttribute('class') == "wpeb_date_start" ? "minDate" : "maxDate",
            var option = minRmax_option,
              instance = jQuery( this ).data( "datepicker" ),
              date = jQuery.datepicker.parseDate(
                instance.settings.dateFormat ||
                jQuery.datepicker._defaults.dateFormat,
                selectedDate, instance.settings );
            dates.not( this ).datepicker( "option", option, date );
          }
        });
      });
    });
  </script>

  <script type='text/javascript'>
          jQuery( document ).ready(function(){            
              jQuery('#wpeb-search-form-3').on('submit', function( e ){   
                  if( jQuery('#wpeb_date_start-3').val() == '' || jQuery('#wpeb_date_end-3').val() == '' ){
                      e.preventDefault();
                      jQuery( '#empty-dates-msg-3' ).fadeIn(400).delay(5000).fadeOut(400);
                      return false;
                  }               
              });
          });
  </script>

  <script type="text/javascript" src="<?php site_url(); ?>/wp-content/plugins/wp-easybooking/widgets/wpeb-search/assets/js/persons-select-script.js"></script>

  <script type='text/javascript'>
  /* <![CDATA[ */
  var WPEB_SEARCH_WIDGET_PERSONS = {"roomsStr":"Room","adultsStr":"Adults","childrenStr":"Children","babiesStr":"Babies","anyStr":"any"};
  /* ]]> */
  </script>

<!-- Skrypty dotyczące pluginu rezerwacyjnego koniec -->

<div class="content no-sidebar">
    <div class="search-form left-column blue paragraph with-list">
        <h2>szukaj noclegu</h2>
        <!-- WYSZUKIWANIE -->
        <?php echo do_shortcode('[wpeb_search_box id="accomodation_search_box" placeholder="nazwa hotelu" persons="yes"]') ?>
        <a href="index.php?page_id=123">
          <button class="blue regular border-button see-all-resorts">zobacz wszytkie obiekty</button>
        </a>
    </div>
    <div class="right-column">
      <div class="blue paragraph">
          <h2>polecane obiekty:</h2>
          <?php get_content();?>
      </div>
    </div>
</div>
</div>
</div></div></div>

<?php get_footer(); ?>