<?php get_header(); ?>

    <section id="wrapper">
        <div id="first" class="slide">
            <div class="slider-content">
                <h1>Hasło reklamowe<br/><small>Trochę dłuższy podtytuł</small></h1>
                <button class="round-big transparent">
                </button>
            </div>
        </div>
        <div id="second" class="slide">
            <div class="slider-content">
                <h1>Drugie hasło<br/><small>Drugi tekst</small></h1>
                <button class="round-big orange">
                </button>
            </div>
        </div>
        <div id="third" class="slide">
            <div class="slider-content">
                <h1>Trzecie hasło<br/><small>Trzeci tekst</small></h1>
                <button class="round-big orange">
                </button>
            </div>
        </div>
    </section>

            <div id="bottombar" class="orange">
                <?php 
                    echo do_shortcode('[wpeb_search_box id="index_search_box" placeholder="nazwa hotelu" persons="no"]')
                ?>
            </div>

    <section id="select-category" class="blue paragraph">
        <div class="icons icons-1 blue">
            <div class="category">
                <div class="category-field">
                   <a href="index.php?page_id=289"><img src="<?php bloginfo('template_directory'); ?>/images/sport.svg"></a>
                </div>
                <h3>SPORT</h3>
                <span>obiekty położone blisko stoków narciarskich i innych obiektów sportowych</span>
            </div>
            <div class="category">
                <div class="category-field">
                    <a href="index.php?page_id=291"><img class="seniors" src="<?php bloginfo('template_directory'); ?>/images/relax.svg"></a>
                </div>
                <h3>WYPOCZYNEK</h3>
                <span>obiekty z bogatą ofertą wypoczynkową</span>
            </div>
            <div class="category">
                <div class="category-field">
                    <a href="index.php?page_id=297"><img src="<?php bloginfo('template_directory'); ?>/images/group.svg"></a>
                </div>
                <h3>GRUPY</h3>
                <span>obiekty posiadające ofertę dla grup zorganizowanych</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="icons icons-2 blue">
            <div class="category">
                <div class="category-field">
                    <a href="index.php?page_id=316"><img src="<?php bloginfo('template_directory'); ?>/images/family.svg"></a>
                </div>
                <h3>RODZINA</h3>
                <span>obiekty posiadające specjalną ofertę dla rodzin z dziećmi</span>
            </div>
            <div class="category">
                <div class="category-field orange">
                    <a href="index.php?page_id=295"><img class="seniors" src="<?php bloginfo('template_directory'); ?>/images/seniors.svg"></a>
                </div>
                <h3>SENIORZY</h3>
                <span>obiekty posiadające specjalną ofertę dla seniorów</span>
            </div>
            <div class="category">
                <div class="category-field">
                    <a href="index.php?page_id=293"><img class="animals" src="<?php bloginfo('template_directory'); ?>/images/dog.svg"></a>
                </div>
                <h3>ZWIERZĘTA</h3>
                <span>obiekty zezwalające na trzymanie zwierząt</span>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>

<?php get_footer(); ?>