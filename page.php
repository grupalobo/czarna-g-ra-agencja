<?php
/*
Template Name: Page
Template do wyświetlania standardowej strony
*/
get_header();
get_sidebar();
?>

<!-- Skrypty dotyczące pluginu rezerwacyjnego -->

  <script type="text/javascript">
    jQuery(function() {
      jQuery('.wpeb_search-widget_frm').each(function(){
        
        var widgetID =  jQuery(this).attr('class').replace('wpeb_search-widget_frm','');

        widgetID = widgetID.replace(' ','');
        var formClass = '.'+jQuery(this).attr('class').replace(' ','.');
        var widgetNum = widgetID.replace('wpeb-search-widget-', '');
        
        //var widgetNum =  jQuery(this).attr('data-wpeb-search-id');
        //var formClass =  widgetNum;
        
        
        var dates = jQuery( formClass+"  .wpeb_date_start, "+formClass+" .wpeb_date_end" ).datepicker({
          defaultDate: "+1w",
          dateFormat : 'dd M yy',
          changeMonth: true,
          numberOfMonths: 1,
          minDate: 0,
          onSelect: function( selectedDate ) {
            //alert( this.getAttribute('class') );
            //var option = this.id == "wpeb_date_start" ? "minDate" : "maxDate",
            var minRmax_option = "maxDate";
            /*if( this.getAttribute('class') == "wpeb_date_start" ) minRmax_option = "minDate";
            if( this.getAttribute('class') == "wpeb_date_end" ) minRmax_option = "maxDate";*/
            if( this.id == 'wpeb_date_start-'+widgetNum ) minRmax_option = "minDate";
            if( this.id == 'wpeb_date_end-'+widgetNum ) minRmax_option = "maxDate";
            //var option = this.getAttribute('class') == "wpeb_date_start" ? "minDate" : "maxDate",
            var option = minRmax_option,
              instance = jQuery( this ).data( "datepicker" ),
              date = jQuery.datepicker.parseDate(
                instance.settings.dateFormat ||
                jQuery.datepicker._defaults.dateFormat,
                selectedDate, instance.settings );
            dates.not( this ).datepicker( "option", option, date );
          }
        });
      });
    });
  </script>

  <script type='text/javascript'>
    jQuery( document ).ready(function(){            
        jQuery('#wpeb-search-form-3').on('submit', function( e ){   
            if( jQuery('#wpeb_date_start-3').val() == '' || jQuery('#wpeb_date_end-3').val() == '' ){
                e.preventDefault();
                jQuery( '#empty-dates-msg-3' ).fadeIn(400).delay(5000).fadeOut(400);
                return false;
            }               
        });
    });
  </script>

  <!-- Skrypty dotyczące pluginu rezerwacyjnego koniec -->

        <section class="content">
            <?php get_content(); ?>
        </section>

<?php get_footer(); ?>