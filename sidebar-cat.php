<!-- Sidebar z menu kategorii do sekscji nieruchomości -->
<div class="orange ontop paragraph sidebar">
        <h4>Wybierz interesującą Cię kategorię:</h4>
        <?php
            $menu_settings = array(
                'theme_location'  => 'sidebar-menu',
                'menu'            => '',
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => '',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            );
            wp_nav_menu($menu_settings);
        ?>
</div>
