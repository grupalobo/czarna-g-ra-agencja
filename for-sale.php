<?php /* Template Name: For sale */ ?>
<?php get_header(); ?>

        <div class="content">
            <div class="left-column-alone">
              <div class="blue paragraph with-list">
                  <h2>Nieruchomości na sprzedaż</h2>
                      <?php
                      $temp = $wp_query; $wp_query= null;
                      $wp_query = new WP_Query(); $wp_query->query('showposts=5' . '&paged='.$paged);
                      while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                        <li class="relative">
                            <div class="post-thumbnail fr">
                            <?php if ( has_post_thumbnail() ) { // check if the post Thumbnail
                            the_post_thumbnail();
                            } ?>
                            </div>
                            <div class="post-short fl">
                              <a href="<?php the_permalink(); ?>">
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt();?>
                                <button class="orange regular short">szczegóły</button>
                              </a>
                            </div>
                            <div class="post-meta">
                              <span>cena:</span>
                              <?php $key_1_value = get_post_meta( get_the_ID(), 'cena', true );
                                if ( ! empty( $key_1_value ) ) {
                                echo $key_1_value;}
                              ?>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <hr class="margined"/>

                      <?php endwhile; ?>



                      <?php wp_reset_postdata(); ?>
                      </div>
                  </div>
            </div>
        </div>

<?php get_sidebar(cat); ?>
<?php get_footer(); ?>