<?php
/*
Template Name: Search Page
Template do wyświetlania wyników wyszukiwania hoteli
*/
get_header();
?>

    <section class="content no-sidebar">
        <div class="search-form left-column">
            <div class="blue paragraph with-list">
                <h2>szukaj noclegu</h2>
                <!-- WYSZUKIWANIE -->
                <?php echo do_shortcode('[wpeb_search_box id="accomodation_search_box" placeholder="nazwa hotelu" persons="yes"]')?>
                <a href="index.php?page_id=123"><button class="blue regular border-button see-all-resorts">zobacz wszytkie obiekty</button></a>
            </div>
        </div>
        <div class="search-results right-column">
        <div class="blue paragraph">
        
            <?php
                $s=get_search_query();
                $args = array(
                                's' =>$s
                            );
                    // The Query
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) {
                        _e("<h2>Wyniki wyszukiwania: ".get_query_var('s')."</h2>");

                        while ( $the_query->have_posts() ) {
                           $the_query->the_post();
                                ?>
                                    <li>
                                        <div class="post-thumbnail fr">
                                        <?php if ( has_post_thumbnail() ) { // check if the post Thumbnail
                                            the_post_thumbnail();
                                        } ?>
                                        </div>
                                        <div class="post-short fl">
                                                <a href="<?php the_permalink(); ?>">
                                                    <h3><?php the_title(); ?></h3>
                                                    <?php the_excerpt();?>
                                                    <button class="orange regular short">zarezerwuj miejsce</button>
                                                </a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <hr/>
                                <?php
                        }
                    }else{
                ?>
                    <h2>Brak wyników</h2>
                    <div class="alert alert-info">
                      <p>Niestety nie posiadamy oferty odpowiadającej Twoim oczekiwaniom. Sprobuj poszukać pokoi dostępnych w innym terminie!</p>
                    </div>
            <?php } ?>
            
        </div>
        </div>
        <div class="clearfix"></div>
    </section>

    <!-- Skrypty dotyczące pluginu rezerwacyjnego -->

    <script type="text/javascript" src="<?php site_url(); ?>/wp-content/plugins/wp-easybooking/widgets/wpeb-search/assets/js/persons-select-script.js"></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var WPEB_SEARCH_WIDGET_PERSONS = {"roomsStr":"Room","adultsStr":"Adults","childrenStr":"Children","babiesStr":"Babies","anyStr":"any"};
        /* ]]> */
    </script>

    <!-- Skrypty dotyczące pluginu rezerwacyjnego koniec -->

<?php get_footer(); ?>