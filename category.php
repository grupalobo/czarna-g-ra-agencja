<?php
  /*
  Template Name: Category
  Template używany do wyświetlania kategorii w dziale nieruchomości
  */
  get_header();
  get_sidebar(cat);
?>

<section class="content">
    <div class="left-column-alone">
      <div class="blue paragraph with-list">
          <h2>Nieruchomości w kategorii <?php single_cat_title( '', true ); ?></h2>

          <?php if (have_posts()) : while (have_posts()) : the_post();?>
            <li class="relative">
                <div class="post-thumbnail fr">
                <?php get_post_thumbnail() ?>
                </div>
                <div class="post-short fl">
                  <a href="<?php the_permalink(); ?>">
                    <h3><?php the_title(); ?></h3>
                    <?php the_excerpt();?>
                    <button class="orange regular short">szczegóły</button>
                  </a>
                </div>
                <div class="post-meta">
                  <span>cena:</span>
                  <?php get_estate_price() ?>
                </div>
                <div class="clearfix"></div>
            </li>
            <hr class="margined"/>
          <?php endwhile; endif; ?>
          
          <?php wp_reset_postdata(); ?>
        </div>
      </div>
    </div>
</section>

<?php get_footer(); ?>