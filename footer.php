        <footer>

            <?php 
            $footer_menu_settings = array(
                'theme_location'  => 'footer-menu',
                'menu'            => '',
                'container'       => 'nav',
                'container_class' => '',
                'container_id'    => 'footer-menu',
                'menu_class'      => '',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            );

            wp_nav_menu($footer_menu_settings); 
        ?>
        </footer>

        <?php wp_footer(); ?>
    </body>
</html>