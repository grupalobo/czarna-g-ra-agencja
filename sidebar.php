<!-- Sidebar z wyszukiwaniem i pogodą -->
<div class="orange ontop paragraph sidebar">
        <h4>Nowa rezerwacja</h4>
        <!-- WYSZUKIWANIE -->
        <?php echo do_shortcode('[wpeb_search_box id="sidebar-search" placeholder="nazwa hotelu" persons="no" show_text_box="yes"]') ?>
</div>
<div class="orange ontop sidebar weather">
    <h4>Czarna Góra: aktualna pogoda</h4>
    <?php echo do_shortcode('[wpc-weather id="75"]'); ?>
</div>

